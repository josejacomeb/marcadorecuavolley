# Open-source Electronic Ecuavolley's  Scoreboard
Ecuavolley is one of the most popular sports in Ecuador, although it's different than the traditional indoor volleyball, I enjoy practicing it with my friends. It requires two teams of three people and as a curiosity it's played with a soccer ball⚽.
The rules are simple, for score a point, an attacking team needs score twice in a row.
To win a game, a team needs to win two sets, the max score per set is 12, however in case of a draw, a third set of 15 points is neccesary to be played.

Traditionally, the score is annotated in small wooden boards or in the ground depending of the surface. For that, I decided to build this to provide a more visual experience for the public also help to matches without a judge to be counted by the players.

Developed by [josejacomeb](josejacomeb@gmail.com)

![Electronic Ecuavolley's Scoreboard](Docs/Images/Ecuaboard.png)

## Materials
- 1 PIC16F628A Micro-controller
- 2 Shift registers 74LS164
- 2 7-seg display 2 digits, common anode
- 4 PNP Transistor 2N3906
- 1 NPN Transitor 2N3904
- 4 LEDs
- 1 Piezoelectric buzzer
- 4 resistor 10KΩ 1/4W   
- 4 resistors 1KΩ 1/4W
- 15 resistors 300 Ω 1/4W
- 1 Electrolytic capacitor 10μF
- 1 Barrel Jack connector
- 3 SPST Tactile Switches
- 1x6 Male Pin Header right angle

## Electrical Characteristics
- _Supply voltage:_ 5V
- _Current drawn:_ To be calculated
- _AA Battery duration:_ To be calculated

## Software Used
- [MPLAB X IDE v5.50](https://www.microchip.com/en-us/tools-resources/develop/mplab-x-ide)
- [MPLAB XC8 Compiler v2.32](https://www.microchip.com/en-us/tools-resources/develop/mplab-xc-compilers)
- [KiCad EDA 6.0](https://www.kicad.org/)
- [FreeCAD 0.19](https://www.freecadweb.org/)
- [kicadStepUp-WB](https://github.com/easyw/kicadStepUpMod)
- [FreeCAD Assembly4 v0.11.10](https://github.com/Zolko-123/FreeCAD_Assembly4)

## How to add support for more sports?
Please read the following file [Adding support to another sports](Electronic-Sports-Scoreboard.X/sports/README.md)

## TODO List

- [ ] Use Surface Mount components
- [ ] Add a potentiometer to adjust the buzzer's volume
- [ ] Make a wireless control
- [ ] Use the EEPROM to save the score-board properties in case of electrical outage
- [X] ~Replace barrel jack connector with a angled molex in the back part of the PCB~
- [X] ~Move the buttons to the upper part of the Case~
- [X] ~Design a case for the prototype~
- [X] ~Add sound alert when it's approaching to the last point~
- [X] ~Define pointers to function to add support for more sports~
- [X] ~Code refactor the Ecuavolley functions into a single library~
- [X] ~Use the Timer 1 to produce sounds~
- [X] ~Change Speaker's Transistor position~
- [X] ~Add a marquee to show text~
- [X] ~Check for ICSP recommended connections guidelines~
- [X] ~Add a control to disable the sound's alerts~
