/* 
 * File:   music_generator.h
 * Author: josejacomeb
 *
 * Created on January 28, 2022, 7:20 PM
 */
//Based in https://microcontrollerslab.com/pwm-using-pic16f877a-microcontroller/ 
#ifndef MUSIC_GENERATOR_H
#define	MUSIC_GENERATOR_H

#include <xc.h>

#define _XTAL_FREQ 4000000UL

const unsigned int *melody_vector;
const unsigned char *duration_vector;
unsigned char vector_index = 0;
unsigned char melody_cycles_counter = 0;
unsigned char prescaler = 16;
unsigned int ten_bits_duty_cycle = 0;

__bit playing_melody = 0;
__bit starting_melody = 0;


void setup_pwm_frequency(const unsigned int * frequency);
void play_melody(const unsigned int *melody, const unsigned char *duration);
void stop_melody();
void update_melody(void);

//Limits
const unsigned int B_3 = 247, B_5 = 990, B_7 = 3952;
#endif	/* MUSIC_GENERATOR_H */
