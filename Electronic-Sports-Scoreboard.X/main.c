/*
 * File:   main.c
 * Author: Jos� J�come
 *
 * Created on March 18, 2019, 12:08 PM
 */

// PIC16F628A Configuration Bit Settings

// 'C' source line config statements

// CONFIG
#pragma config FOSC = INTOSCIO  // Oscillator Selection bits (INTOSC oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = OFF      // RA5/MCLR/VPP Pin Function Select bit (RA5/MCLR/VPP pin function is digital input, MCLR internally tied to VDD)
#pragma config BOREN = OFF      // Brown-out Detect Enable bit (BOD disabled)
#pragma config LVP = OFF        // Low-Voltage Programming Enable bit (RB4/PGM pin has digital I/O function, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection bit (Data memory code protection off)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

//#include <xc.h>

#include "scoreboard_marquee.h"
#include "music_generator.h"
//#include "texts.h"
#include "constants.h"
#include "definitions.h"
//Include sports function
#include "sports/volleyball.h"


//PORTA I/O pins
#define R_S_R PORTAbits.RA0 //Right Side Shift Register Physical Pin 17 I/O?: 0
#define RIGHT_COUNTER PORTAbits.RA1 //Pyshical Pin 18 I/O?: 1
#define RE_SET PORTAbits.RA2 //Physical Pin 1 I/O?: 1
#define L_S_R PORTAbits.RA3 //Left Side Shift Register Physical Pin 2 I/O?: 0
#define D1 PORTAbits.RA4 //Digit 1 Physical Pin 3 I/O?: 0
#define LEFT_COUNTER PORTAbits.RA5 //Physical Pin I/O?: 1
#define CLK PORTAbits.RA6 //Physical Pin 15 I/O?: 0
#define D2 PORTAbits.RA7 //Digit 2 Physical Pin 16 I/O?: 0

//#define RST PORTBbits.RB0 //Reset Shift registers Digital Pin 6
#define SPEAKER PORTBbits.RB3
#define LEFT_SET_LED PORTBbits.RB4 //Physical Pin 10
#define RIGHT_SET_LED PORTBbits.RB5 //Physical Pin 11
#define RIGHT_SERVE_LED PORTBbits.RB6 //Physical Pin 12
#define LEFT_SERVE_LED PORTBbits.RB7 //Physical Pin 13

#define _XTAL_FREQ 4000000UL

__bit current_digit = 0;

struct visual_indicators scoreboard_indicators;

__bit set_parameters = 0;

void mostrar(volatile char* izquierda, volatile char* derecha);


#define TIMER0 
//Code inspired in https://saeedsolutions.blogspot.com/2014/02/pic16f628a-timer0-code-proteus.html 
#ifdef TIMER0

void Init_Timer0() {
    //8 Bits timer counter
    //Make prescaler 1:32
    OPTION_REG &= 0xC4; //Proteus 0xC7; Normal: 0xC4 
    GIE = 1; //Enable global interrupts
    T0IE = 1; //Enable Timer0 interrupts
}

void __interrupt() scoreboard_interrupts(void) {
    if (T0IF) {
        D1 = 1;
        D2 = 1;
        if (current_digit) { //Tens cycle
            mostrar(&scoreboard_indicators.left_digit_1, &scoreboard_indicators.right_digit_1);
            D1 = 0;
        } else {//Units cycle
            mostrar(&scoreboard_indicators.left_digit_2, &scoreboard_indicators.right_digit_2);
            D2 = 0;
        }
        if (current_digit) current_digit = 0;
        else current_digit = 1;
        T0IF = 0;
    }
    if (TMR1IF) {
        if (marquee_activated)
            update(&scoreboard_indicators.left_digit_1, &scoreboard_indicators.left_digit_2,
                &scoreboard_indicators.right_digit_1, &scoreboard_indicators.right_digit_2);
        if (starting_melody)
            update_melody();
        TMR1IF = 0;
    }
}
#endif

#define TIMER1
//Code inspired in https://saeedsolutions.blogspot.com/2014/02/pic16f628a-timer0-code-proteus.html 
#ifdef TIMER1

void Init_Timer1() {
    //16 bits timer counter 
    //Frecuency of every timer count 1e-6s, overflow at 65ms
    PEIE = 1; //Enable Peripheral Interrupts
    T1CKPS1 = 0; //T1CKPS 1:1 Prescaler value 
    T1CKPS1 = 0;
    T1OSCEN = 0; //Disable Timer 1 Oscillator
    TMR1CS = 0; //Select internal clock
    TMR1IE = 1; //Enable the Timer1 interrupt
    TMR1ON = 1; //Enable Timer 1
}

#endif

void main(void) {
    CMCONbits.CM = 0b111; //Disable comparator  
    TRISA = 0x26; //Configure inputs and outputs 
    TRISB = 0x00;
    PORTB = 0;
    D1 = 1;
    D2 = 1;
    LEFT_SET_LED = 0;
    RIGHT_SET_LED = 0;
    T0IF = 0; //Initialize the counter
    //PWM_registers registers = {T2CKPS0, T2CKPS1, TMR2ON, CCP1M2, CCP1M3, CCP1Y, CCP1X, &PR2, &CCPR1L};
    Init_Timer0();
    Init_Timer1();
    //init_pwm(); //Start the PWM configuration
    scoreboard_indicators.LEDs.sound_enabled = 1; //Enable Speaker
    //Pointer to a function
    void (*setup_indicators)(struct visual_indicators*) = volleyball_setup_indicators;
    void (*decrease_left)(void) = volleyball_decrease_left;
    void (*decrease_right)(void) = volleyball_decrease_right;
    void (*increase_left)(void) = volleyball_increase_left;
    void (*increase_right)(void) = volleyball_increase_right;
    void (*reset)(void) = volleyball_reset;

    //Setup Digits and LEDs according to the sport
    setup_indicators(&scoreboard_indicators);
    for (;;) {
        if (!LEFT_COUNTER) {
            __delay_ms(500);
            if (LEFT_COUNTER) {
                increase_left();
            } else {
                decrease_left();
            }
            __delay_ms(500);
        }
        if (!RIGHT_COUNTER) {
            __delay_ms(500);
            if (RIGHT_COUNTER) {
                increase_right();
            } else {
                decrease_right();
            }
            __delay_ms(500);
        }
        //Code to set the sport's preference 
        if (!RE_SET) {
            __delay_ms(300);
            if (RE_SET) {
                set_parameters = (set_parameters == 1) ? 0 : 1;
                //setText(test, 13);
            } else {
                //If the button is pressed, reset all the parameters 
                reset();
            }
            __delay_ms(500);
        }
        RIGHT_SERVE_LED = (__bit) scoreboard_indicators.LEDs.upper_right;
        LEFT_SERVE_LED = (__bit) scoreboard_indicators.LEDs.upper_left;
        RIGHT_SET_LED = (__bit) scoreboard_indicators.LEDs.lower_right;
        LEFT_SET_LED = (__bit) scoreboard_indicators.LEDs.lower_left;
        // TODO
        /*
        if (set_parameters == 1) {
        }
         */
    }
    return;
}

void mostrar(volatile char* izquierda, volatile char* derecha) {
    unsigned char bit_mask = 0;
    for (unsigned char i = 0; i < 0x08; i++) {
        bit_mask = (unsigned char) (0x01 << i);
        CLK = 0;
        __delay_us(1);
        L_S_R = ascii_table[*izquierda] & (bit_mask) ? 1 : 0;
        R_S_R = ascii_table[*derecha] & (bit_mask) ? 1 : 0;
        CLK = 1;
        __delay_us(1);
    }
}