# Support for more sports
This project is intended to be a fully capable sports scoreboard. Through the use of a two seven segment displays, 2 LEDs you can give to your sport the desired functionality according to its own rules. The support of the sports are defined in separated file inside the 📁 `sports` folder, please define your variables and prototype of functions inside the `desired_sport.h` header file and the functionality inside the `desired_sport.c` file. Finally, link them inside the MPlab X program to compile the final program.

## Common interface for add more sports
For make it handy, I've defined a common struct in the  [definitions.h](./../definitions.h) file. The prototype of the struct is the following with its members:

```C
struct visual_indicators {

    struct StatusLEDs {
        unsigned right_serve : 1;
        unsigned left_serve : 1;
        unsigned right_set : 1;
        unsigned left_set : 1;
        unsigned sound_enabled : 1;
        unsigned marquee_enabled : 1;
    } LEDs;
    volatile char left_digit_1; //Tens
    volatile char right_digit_1;
    volatile char left_digit_2; //Ones
    volatile char right_digit_2;
};
```
The members of the struct `visual_indicators` are the following:
- `struct StatusLEDs`: Bit-field structure that holds the status of the scoreboard's LEDs and user preferences
  - `upper_right`: Status of the Upper Right LED
  - `upper_left`: Status of the Upper Left LED
  - `lower_right`: Status of the Lower Right LED
  - `left_set`: Status of the Lower Left LED
  - `sound_enabled`: User defined preference to make sound on every action of the scoreboard
  - `marquee_enabled`: User defined preference to show the marquee effect in the 7 segment display.
- `volatile char left_digit_1`: Holds the character corresponding to the tens digit of the left 7 segment display
- `volatile char left_digit_2`: Holds the character corresponding to the units digit of the left 7 segment display
- `volatile char right_digit_1`: Holds the character corresponding to the tens digit of the right 7 segment displays
- `volatile char right_digit_2`: Holds the character corresponding to the units digit of the right 7 segment display

## Minimum methods to provide support to your sport
Because the Scoreboard has three buttons, `left` and `right` buttons increase and decrease the counter, so they'll perform four functions. The `re_set` button, sets the user preferences and reset the count, the first one is going to be held by the `main.c` program whilst the other can be handled by your sport's library, so my recommendation is the following template with 6 basic functions.


```C
void custom_sport_setup_indicators(struct visual_indicators *indicators){
/*
 * Add your code here to pass the pointer to a local pointer
 */

}
void custom_sport_reset_counters(void){
/*
 * Code to reset the counters
 */
}
void custom_sport_increase_left(void){
/*
 * Perform your logic after pressing the left button to increase
 */
}
void custom_sport_decrease_left(void){
/*
* Perform your logic after pressing the left button to decrease
 */
}
void custom_sport_increase_right(void){
/*
 * Perform your logic after pressing the right button to increase
 */
}
void custom_sport_decrease_right(void){
/*
 * Perform your logic after pressing the right button to decrease
 */
}
```
Those functions should be pass to as function-pointers in the `main.c` file, to ensure compatibility to add more sports.

For a full implementation of this example, please refer to the [volleyball.h](./../volleyball.h) and [volleyball.c](./../volleyball.c) files in the sport directory.
