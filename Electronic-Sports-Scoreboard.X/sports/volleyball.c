#include "volleyball.h"
#include "../music_generator.h" //Load music generation library 

void volleyball_reset(void) {
    volleyball_left_set = 0;
    volleyball_right_set = 0;
    volleyball_reset_counters();
    volleyball_max_current_set = &volleyball_max_first_second;
    volleyball_set_digits();
}

void volleyball_reset_counters() {
    volleyball_left_counter = 0;
    volleyball_right_counter = 0;
    volleyball_left_serve = 0;
    volleyball_right_serve = 0;
}

void volleyball_check_score() {
    if (volleyball_left_set & volleyball_right_set) {
        volleyball_max_current_set = &volleyball_max_third_set;
    }
}

void volleyball_setup_indicators(struct visual_indicators *indicators) {
    volleyball_indicators = indicators;
}

void volleyball_increase_left(void) {
    if (volleyball_left_serve) {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(increase_point_left, increase_point_left_cycles);
        volleyball_left_counter += 1;
    } else {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(serving_team, serving_team_cycles);
        volleyball_left_serve = 1;
        volleyball_right_serve = 0;
    }
    if (volleyball_left_counter == *volleyball_max_current_set) {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(set_won, set_won_cycles);
        if (volleyball_left_set) //Match won
            volleyball_reset();
        else {
            volleyball_reset_counters();
            volleyball_left_set = volleyball_right_set;
            volleyball_right_set = 1; //Because the teams swap their positions
        }
        volleyball_check_score();
        volleyball_reset_counters();
    } else if (volleyball_left_counter == (*volleyball_max_current_set - 1)) {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(last_point, last_point_cycles);
    }
    volleyball_set_digits();
}

void volleyball_decrease_left(void) {
    if (volleyball_left_counter > 0) {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(decrease_point_left, decrease_point_left_cycles);
        volleyball_left_counter -= 1;
    }
    volleyball_set_digits();
}

void volleyball_increase_right(void) {
    if (volleyball_right_serve) {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(increase_point_right, increase_point_right_cycles);
        volleyball_right_counter += 1;
    } else {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(serving_team, serving_team_cycles);
        volleyball_left_serve = 0;
        volleyball_right_serve = 1;
    }
    if (volleyball_right_counter == *volleyball_max_current_set) {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(set_won, set_won_cycles);
        if (volleyball_right_set)
            volleyball_reset();
        else {
            volleyball_reset_counters();
            volleyball_right_set = volleyball_left_set;
            volleyball_left_set = 1; //Because the teams swap their positions
        }
        volleyball_check_score();
        volleyball_reset_counters();
    } else if (volleyball_right_counter == (*volleyball_max_current_set - 1)) {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(last_point, last_point_cycles);
    }
    volleyball_set_digits();
}

void volleyball_decrease_right(void) {
    if (volleyball_right_counter > 0) {
        if (volleyball_indicators->LEDs.sound_enabled)
            play_melody(decrease_point_right, decrease_point_right_cycles);
        volleyball_right_counter -= 1;
    }
    volleyball_set_digits();
}

void volleyball_set_digits(void) {
    volleyball_indicators->left_digit_1 = (volleyball_left_counter / 10);
    volleyball_indicators->left_digit_2 = (volleyball_left_counter % 10);
    volleyball_indicators->right_digit_1 = (volleyball_right_counter / 10);
    volleyball_indicators->right_digit_2 = (volleyball_right_counter % 10);
    //Use pointer to Bitfields
    volleyball_indicators->LEDs.upper_left = volleyball_left_serve;
    volleyball_indicators->LEDs.upper_right = volleyball_right_serve;
    volleyball_indicators->LEDs.lower_left = volleyball_left_set;
    volleyball_indicators->LEDs.lower_right = volleyball_right_set;
}