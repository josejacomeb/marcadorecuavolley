/* 
 * File:   volleyball.h
 * Author: josejacomeb
 *
 * Created on February 19, 2022, 7:34 PM
 */

#ifndef VOLLEYBALL_H
#define	VOLLEYBALL_H

#include <xc.h>
#include "./../definitions.h"

char volleyball_left_counter = 0, volleyball_right_counter = 0;
__bit volleyball_left_serve = 0, volleyball_right_serve = 0;

struct visual_indicators *volleyball_indicators;
__bit volleyball_left_set = 0, volleyball_right_set = 0;
const char volleyball_max_first_second = 12, volleyball_max_third_set = 15;
const char *volleyball_max_current_set = &volleyball_max_first_second;

void volleyball_setup_indicators(struct visual_indicators *indicators);
void volleyball_reset_counters(void);
void volleyball_reset(void);
void volleyball_increase_left(void);
void volleyball_decrease_left(void);
void volleyball_increase_right(void);
void volleyball_decrease_right(void);
void volleyball_set_digits(void);
void volleyball_check_score(void);

//Serving team
//C_4, E_4, G_4
const unsigned int serving_team[3] = {261, 329, 392};
const unsigned char serving_team_cycles[4] = {13, 13, 13, 0};

//Left increase 
//F_4, D_4 
const unsigned int increase_point_left[2] = {349, 293};
const unsigned char increase_point_left_cycles[3] = {10, 10, 0};

//Left decrease 
//D_5, F_5  
const unsigned int decrease_point_left[2] = {587, 698};
const unsigned char decrease_point_left_cycles[3] = {10, 10, 0};

//Right increase 
//C_5, A_4
const unsigned int increase_point_right[2] = {523, 440};
const unsigned char increase_point_right_cycles[3] = {10, 10, 0};

//Right decrease 
//A_5, C6
const unsigned int decrease_point_right[2] = {880, 1046};
const unsigned char decrease_point_right_cycles[3] = {10, 10, 0};

//Last point 
//G_5, 0, G_5, 0, G_5, 0, D#_5, 0, A#_5, 0, G_5, 0, D#_5, 0, A#_5, 0, G_5  Imperial March Theme
const unsigned int last_point[17] = {784, 0, 784, 0, 784, 0, 622, 0, 932, 0, 784, 0, 622, 0, 932, 0, 784};
const unsigned char last_point_cycles[18] = {7, 2, 7, 2, 7, 2, 5, 2, 1, 2, 7, 2, 5, 2, 1, 2, 7, 0};

//Set won point 
//E_5, D#_5, E_5, D#_5, E_5, B_4, D_5, C_5, A_4 Fur Elise
const unsigned int set_won[9] = {659, 622, 659, 622, 659, 493, 587, 523, 440};
const unsigned char set_won_cycles[10] = {13, 13, 13, 13, 13, 13, 13, 13, 13, 0};

#endif	/* VOLLEYBALL_H */

