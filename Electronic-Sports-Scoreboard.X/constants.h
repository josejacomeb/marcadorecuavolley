/* 
 * File:   constants.h
 * Author: josejacomeb
 *
 * Created on January 26, 2022, 10:30 PM
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

const char ascii_table[38] = {
    //bits
    // 7, 6, 5, 4, 3, 2, 1, 0 
    //DP, g, f, e, d, c, b, a
    //hex, value, char, decimal
    0xC0, //0, 0
    0xF9, //1, 1
    0xA4, //2, 2
    0xB0, //3, 3
    0x99, //4, 4
    0x92, //5, 5
    0x82, //6, 6
    0xF8, //7, 7
    0x80, //8, 8
    0x90, //9, 9
    0x88, //A 10
    0x83, //B 11
    0xC6, //C 12
    0xA1, //D 13
    0x86, //E 14
    0x8E, //F 15
    0xC2, //G 16
    0x89, //H 17
    0xCF, //I 18
    0xF1, //J 19
    0xFF, //K 20 Not implemented
    0xC7, //L 21
    0xC8, //M 22 Not implemented
    0xAB, //N 23
    0xC0, //O 24
    0x8C, //P 25
    0x98, //Q 26
    0xAF, //R 27
    0x92, //S 28
    0x87, //T 29
    0xC1, //U 30
    0xFF, //V 31 Not implemented
    0xFF, //W 32 Not implemented
    0xFF, //X 33 Not implemented
    0x91, //Y 34
    0xA4, //Z 35
    0xFF, //SPACE 36
    0x7F, //Period 37  
};

#endif	/* CONSTANTS_H */
