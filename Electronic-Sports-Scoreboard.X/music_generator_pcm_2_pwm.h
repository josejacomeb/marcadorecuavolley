/* 
 * File:   music_generator.h
 * Author: josejacomeb
 *
 * Created on January 28, 2022, 7:20 PM
 */
//Based in https://microcontrollerslab.com/pwm-using-pic16f877a-microcontroller/ 
#ifndef MUSIC_GENERATOR_H
#define	MUSIC_GENERATOR_H

#include <xc.h>
#include "test.h"


const unsigned char *melody_vector;
unsigned int vector_index = 0;
unsigned int melody_vector_size = 0;
unsigned int ten_bits_duty_cycle = 0;
int data  = 0;

__bit playing_melody = 0;
__bit starting_melody = 0;
__bit enable_speaker = 0;

void init_pwm(void);
void setup_pwm_frequency(const unsigned char * frequency);
void play_melody(const unsigned char *melody);
void stop_melody(void);
void update_melody(void);



#endif	/* MUSIC_GENERATOR_H */

