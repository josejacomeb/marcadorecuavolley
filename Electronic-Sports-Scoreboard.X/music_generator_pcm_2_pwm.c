#include "music_generator.h"

void setup_pwm_frequency(const unsigned char * pcm_value) {
    // 4 transform from 8 to 10 bits, 
    // Equation = pcm_value * (PR2 + 1)/255'
    ten_bits_duty_cycle = (PR2 + 1)*(*pcm_value)/0xFFu;
    ten_bits_duty_cycle *= 4; 
    CCPR1L = (unsigned char) (ten_bits_duty_cycle >> 0x02); //8 MSB
    CCP1X = (__bit) ten_bits_duty_cycle & 0x02; //Bit 5 1st LSB 
    CCP1Y = (__bit) ten_bits_duty_cycle & 0x01; //Bit 4 2nd LSB
}

void init_pwm() {
    //Set in PWM Mode
    CCP1M3 = 1;
    CCP1M2 = 1;
    //Set Prescaler to 1
    T2CKPS1 = 0;
    T2CKPS0 = 0;
    //Frecuency of 4.6KHz, Prescaler = 1. F_OSC = 4Mhz
    PR2 = 217;
    CCPR1L = 0x00; //8 MSB
    CCP1X = 0; //Bit 5 1st LSB 
    CCP1Y = 0; //Bit 4 2nd LSB*/
}

void play_melody(const unsigned char *melody) {
    melody_vector = melody;
    vector_index = 0;
    starting_melody = 1;
}

void stop_melody() {
    setup_pwm_frequency(0);
    TMR2ON = 0; //Turn off Timer 2
    playing_melody = 0;
    vector_index = 0;
    starting_melody = 0;
}

void update_melody(void) {
    if (!playing_melody) {
        TMR2ON = 1;
        playing_melody = 1; //Set as playing melody
    }
    setup_pwm_frequency(&melody_vector[vector_index]);
    if (*(melody_vector + vector_index) == 0) {
        stop_melody();
    }
    vector_index += 1;
}


