#include "music_generator.h"

void setup_pwm_frequency(const unsigned int * frequency) {
    //Between  B3 <= frequency < B5
    if (*frequency >= B_3 && *frequency < B_5) {
        //Set Prescaler to 16
        T2CKPS1 = 1;
        T2CKPS0 = 1;
        prescaler = 16;
    }//Between  B5 <= frequency < B7
    else if (*frequency >= B_5 && *frequency < B_7) {
        //Set Prescaler to 4
        T2CKPS1 = 0;
        T2CKPS0 = 1;
        prescaler = 4;
    }
    if (*frequency > 0) {
        PR2 = (unsigned char) (_XTAL_FREQ / (4 * (*frequency) * prescaler) - 1);
        ten_bits_duty_cycle = 2 * (PR2 + 1); //Use the half of the square wave
    } else {
        ten_bits_duty_cycle = 0;
    }
    CCPR1L = (unsigned char) (ten_bits_duty_cycle >> 0x02); //8 MSB
    CCP1X = (ten_bits_duty_cycle >> 1) & 0x01; //Bit 5 1st LSB 
    CCP1Y = (ten_bits_duty_cycle & 0x01); //Bit 4 2nd LSB
}
//Define in cycles, every cycle means 65ms

void play_melody(const unsigned int *melody, const unsigned char* duration) {
    melody_vector = melody;
    duration_vector = duration;
    melody_cycles_counter = 0;
    vector_index = 0;
    starting_melody = 1;
    //Set in PWM Mode
    CCP1M3 = 1;
    CCP1M2 = 1;
}

void stop_melody() {
    TMR2ON = 0; //Turn off Timer 2
    CCP1CON = 0x00; //Reset and set to LOW the CCP Module
    RB3 = 0; //Force to reset the PIN
    starting_melody = 0;
    playing_melody = 0;
}

void update_melody(void) {
    if (starting_melody) {
        if (!playing_melody) {
            TMR2ON = 1; //Turn on the timer 1
            playing_melody = 1; //Set as playing melody
            setup_pwm_frequency(&melody_vector[vector_index]);
        }
        melody_cycles_counter++;
        if (melody_cycles_counter >= duration_vector[vector_index]) {
            vector_index++;
            if (duration_vector[vector_index]) {
                setup_pwm_frequency(&melody_vector[vector_index]);
                melody_cycles_counter = 0;
            } else {
                stop_melody();
            }
        }
    }
}

