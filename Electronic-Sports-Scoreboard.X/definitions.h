/* 
 * File:   definitions.h
 * Author: josejacomeb
 *
 * Created on February 27, 2022, 12:58 PM
 */

#ifndef DEFINITIONS_H
#define	DEFINITIONS_H

struct visual_indicators {

    struct StatusLEDs {
        unsigned upper_right : 1;
        unsigned upper_left : 1;
        unsigned lower_right : 1;
        unsigned lower_left : 1;
        unsigned sound_enabled : 1;
        unsigned marquee_enabled : 1;
        //Add Support for two ther variables
    } LEDs;
    volatile char left_digit_1; //Tens
    volatile char right_digit_1;
    volatile char left_digit_2; //Ones
    volatile char right_digit_2;
};

#endif	/* DEFINITIONS_H */

