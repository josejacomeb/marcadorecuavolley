/* 
 * File:   scoreboard_marquee.h
 * Author: josejacomeb - 2021
 * Comments:
 * Revision history: 
 */

#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H

#include <xc.h> // include processor files - each processor file is guarded.  

/**
 * Function to init the PIC Frecuency
 * @param uc_frequency
 */

unsigned char cycles_counter = 0;
unsigned char marquee_idx = 0; //Marque index
__bit marque_direction = 0; //0 Goes right, 1 goes left
const char* marquee_text;
unsigned char marquee_cycles_duration = 0;
const unsigned char length_text = 4;
__bit marquee_activated = 0;

void setText(const char* text, unsigned char duration);
void reset_marquee(void);
char conversion_ascii_scoreboard_7seg(char value);
void update(volatile char* l_d_1, volatile char* l_d_2, volatile char* r_d_1, volatile char* r_d_2);

#endif	/* XC_HEADER_TEMPLATE_H */
