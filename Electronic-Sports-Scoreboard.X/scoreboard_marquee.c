/*
 * File:   scoreboard_marquee.c
 * Author: josejacomeb
 *
 * Created on February 15, 2022, 2:26 PM
 */

#include "scoreboard_marquee.h"

void reset_marquee(void) {
    cycles_counter = 0;
    marque_direction = 0;
    marquee_idx = 0;
    marquee_activated = 0; 
}

void setText(const char* text, unsigned char duration) {
    reset_marquee();
    marquee_text = text;
    marquee_cycles_duration = duration;
    marquee_activated = 1;
}


char conversion_ascii_scoreboard_7seg(char value){
    //Conversion for the numbers 
    if(value >= 48 && value <= 57){
        value -= 48;
    }
    else if(value >= 65 && value <= 90) //uppercase latin alphabet
        value -= 55;
    else if(value >= 97 && value <= 122) //lowercase latin alphabet
        value -= 87;
    else if(value == 44 || value == 46)  //Comma and period
        value = 37;
    return value;
}

/**
 * Function to update 
 */
void update(volatile char* l_d_1, volatile char* l_d_2, volatile char* r_d_1, volatile char* r_d_2) {
    cycles_counter ++;
    if (cycles_counter > marquee_cycles_duration) {
        if (marque_direction) { //Going left
            if (marquee_idx > 0) {
                marquee_idx--;
            } else {
                marque_direction = 0;
            }
        } else { //Going right
            //Check if the marquee reached the last element of the array
            if (*(marquee_text + marquee_idx + length_text) != '\0') { 
                marquee_idx++;
            } else {
                marque_direction = 1;
            }
        }
        cycles_counter = 0;
    }
    *l_d_1 = conversion_ascii_scoreboard_7seg(*(marquee_text + marquee_idx));
    *l_d_2 = conversion_ascii_scoreboard_7seg(marquee_text[marquee_idx + 1]);
    *r_d_1 = conversion_ascii_scoreboard_7seg(marquee_text[marquee_idx + 2]);
    *r_d_2 = conversion_ascii_scoreboard_7seg(marquee_text[marquee_idx + 3]);
}

