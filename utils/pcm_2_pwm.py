#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
josejacomeb@gmail.com
"""

import argparse

import numpy as np
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import samplerate

def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", '--input', required=True,
                        help="Path to the PCM input file")
    parser.add_argument("-o", '--output', required=True,
                        help="Name of the output header file, e.g. my_sound.h")
    parser.add_argument("--converter", help="Sampling mode(linear, sinc_best, sinc_fastest, zero_order_hold)", default="linear")
    parser.add_argument("--show", help="Show the result graphs of the waveform ", default=False)

    return parser

def array_to_header(array, header_file):
    NUMBER_OF_CHARACTERS_ENTER = 75
    name_array = header_file.split(".")[0]
    with open(header_file, "w") as file:
        file.write(f"#ifndef {name_array.upper()}_H\n")
        file.write(f"#define {name_array.upper()}_H\n")
        file.write(f"const char {name_array}[{len(array)}] = """ + "{\n")
        text = "\t"
        for frequency in array:
            text += hex(frequency)
            text += ", "
            if len(text) >= NUMBER_OF_CHARACTERS_ENTER:
                text += "\n"
                file.write(text)
                text = "\t"
        if text:
            text = text[0:-2]
            file.write(text)
        file.write("\n};\n#endif")

def main():
    args = build_argparser().parse_args()  # Get the args from the command line
    if not "raw" in args.input:
        print("Please, specify as input file a PCM uint8 raw file")
        return -1
    input_frequency = int(input("The input frequency of the file is(Hz): "))
    output_frequency = int(input("The output frequency of the file is(Hz): "))
    input_data = np.memmap(args.input, mode='r')
    ratio  = output_frequency/input_frequency
    if ratio < 1/256:
        temp_frecuency = input_frequency
        temp_data_sample = input_data
        end_loop = False
        while True:
            ratio = output_frequency/temp_frecuency
            if ratio < 1/256:
                ratio = 1/256
            else:
                end_loop = True
            print(ratio)
            temp_data_sample = samplerate.resample(temp_data_sample, ratio, args.converter)
            if end_loop:
                print("Break")
                break
            temp_frecuency = temp_frecuency*ratio
            print(f"temp_frecuency = {temp_frecuency}")
        output_data_sample = temp_data_sample
    else:
            output_data_sample = samplerate.resample(input_data, ratio, args.converter)

    output_data_sample = output_data_sample.astype(np.uint8)
    array_to_header(output_data_sample, args.output)
    if args.show:
        fig = make_subplots(rows=2, cols=1)
        fig.add_trace(go.Scatter(x=np.arange(len(input_data)), y=input_data, mode='lines',
                name=f'Melody sampled at {input_frequency}Hz'), row=1, col=1)
        fig.add_trace(go.Scatter(x=np.arange(len(output_data_sample)), y=output_data_sample,
                mode='lines', name=f'Melody sampled at {output_frequency}Hz'), row=2, col=1)
        fig.show()
if __name__ == "__main__":
    main()
